## Ocenianie projektów

> sory, dałbyś mi tak 10min? dzięki

> Odkryłem, że macie egzaminy

> Ci, którzy wybrali łatwiejsze tematy, dostaną niższą ocenę

> U mnie zdawalność jest bardzo niska

> Wiesz o co mi chodzi

![wymogi projektowe](https://cdn.discordapp.com/attachments/852864426213113886/933735931150729277/unknown.png)
![interview](https://cdn.discordapp.com/attachments/852864426213113886/935122990339866664/unknown.png)


## LinkedIn

> I am student of Silesian University of Technology, and I want to do PhD at
> the university. Since I was a boy, I was interested in Computers and how they
> work. This is the reason why did these projects. Additionally, I am a
> disciplined student, so I have never skipped any lecture or laboratory at the
> university because I believe that university is a place to study and to
> gained knowledge through lectures and laboratories. This is the reason, I
> have studied computer architecture and did projects at home to develop my
> knowledge further in this area. I am huge fan of Linux, because of its
> simplicity. I am regular user of Debian; however I have used Gentoo because I
> wanted to learn more about Linux itself through practice. During free time, I
> read philosophy, theology, Linux, kernel development, embedded systems,
> computer architecture, PLC, verilog and C/C++ programming. I believe that
> reading is also important to gain knowledge about a certain subject as well
> as putting this knowledge into practice. The reason, I have decided to study
> Electronics and Telecommunication Engineering, after my Bachelor’s in
> Computer Engineering was that the Master’s in Electronics and
> Telecommunication Engineering had courses in PLC programming, Verilog
> programming, ARM programming and Embedded Linux.
